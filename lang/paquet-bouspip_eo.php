<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bouspip?lang_cible=eo
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouspip_description' => 'Tiu kromprogramo provizas la nomon, la retadreson kal la priskribon de ĉiuj retejoj de la SPIP-Galakcio. Oni devas uzi ĝin per la kromprogramo Orientigilo en servila funkciado por disdoni, tra reta servo, ĝiajn informojn al ĉiuj retejoj kiuj deziras afiŝi la SPIP-orientigilon.',
	'bouspip_nom' => 'SPIP orientigilo',
	'bouspip_slogan' => 'La bonaj adresoj de la SPIP-galakcio !'
);
