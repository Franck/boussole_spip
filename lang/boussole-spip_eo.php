<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/boussole-spip?lang_cible=eo
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'descriptif_boussole_spip' => 'La SPIP-kompaso kunigas la aron de « oficialaj » retejoj de la SPIP galaksio. Ĝi registras pri ĉiu retejo ĝian vinjeton, ĝian nomon, ĝian devizon kaj ĝian priskribon. Do ne hezitu uzi ĝin ĉe viaj propraj retejoj por orientigi viajn vizitantojn tra la SPIP galaksio.',
	'descriptif_site_spip_blog' => 'Ĉar SPIP estas kunlaboriga projekto, spip-blog.net kolektas teknikajn artikoletojn, mem mokadojn, trolojn, diversajn anoncojn , … Pro tio ĝi bone respegulas la SPIP komunumon : unue kaj precipe multege de tenero.',
	'descriptif_site_spip_contrib' => 'Kiel kunlaboriga retejo, contrib.spip disponigas la tuton de la exteraj kontribuaĵoj : kromprogramojn, skriptojn, filtrilojn, skeletojn, dokumentojn, trukojn kaj elturnigilojn,… el la komunumo (elŝutligoj) far la SPIP uzantoj. Ĝiaj forumoj plenumas la ligon inter konceptistoj kaj uzantoj.',
	'descriptif_site_spip_core' => 'La retejo CORE.SPIP.net arigas la historion de la modifoj de la SPIP kodo, mastrumadon de la petoslipoj kiuj ebligas averti pri cimoj kaj proponi sugestojn por plibonigado, kaj elŝuta spaco de la SPIP versioj.',
	'descriptif_site_spip_demo' => 'Retejo de testo ĝisdatigita ĉiunokte, demo.spip.net ebligas al ĉiu provi SPIP-on en ĝia plej nova stabila versio (kun, elekteble, la redakta aŭ mastrumada statuso), per nura alklako kaj sen instali ĝin. ',
	'descriptif_site_spip_doc' => 'CODE.SPIP.net estas dokumentiga spaco de la SPIP-programaro por siaj interfacoj je aplika programado (API), sia fontokodo, kaj kelkaj el siaj teknikaj funkciiloj.',
	'descriptif_site_spip_edgard' => 'Edgard estas nelacigebla kaj fidela kompano de SPIP tuja babilejo kie ĝi ĉiam intervenas taŭge, tenere kaj humure. El sia domo edgard.spip.net ĝi portas al IRC siajn konsilojn, respondojn kaj gajecon. Cetere, Edgard estas roboto (sed ĝi sajnigas ne konsii pri tio).',
	'descriptif_site_spip_forum' => 'Forum.spip.net estas la retejo por interŝanĝo kaj interhelpo de la SPIP uzantoj. Tiu retejo ekzistas en deko da lingvoj kaj dispartiĝas laŭ kvar grandaj rubrikoj : instalado kaj ĝisdatigo, uzado de la privata spaco, bontenado, mastrumado, retejagordo, kreado de skeletoj.',
	'descriptif_site_spip_herbier' => 'La SPIP herbaro ne estas ligaro kiel aliaj. Ne. Estas memoralbumo de retejoj efektivigitaj per SPIP, kaptitaj je certa momento de ilia ekzisto ĉe la Reto. Ĝi rakontas pri la reta historio, tiu de SPIP ekde 2001. Tiu herbaro arigas la retejojn kiujn ni emis afiŝi, ĉar interesaj laŭ ia enhavo agrable valorigita, ia homa aventuro, ia lerte uzita funksiigilo, aŭ nur vide allogaj. Estas retejoj pri kiuj oni memoras, ĉe kiuj oni revenas, kiu estis referencoj signante ilian epokon iel aŭ tiel.',
	'descriptif_site_spip_info' => 'En kelkaj paĝoj, SPIP-INFO.net respondas la demandojn metitajn de geuzantoj kiuj evaluas la kapablecon de SPIP servi kiel bazo de ilia disvolviga projekto por retejo. Privatuloj, asocioj, entreprenoj trovos tie ĉi ĉiujn informojn por apogi ilian elekton.',
	'descriptif_site_spip_irc' => 'La SPIP-komunumo, neniam dormante, dotiĝis per IRC-kanalo (senprokrasta diskutado per Interreto) malfermita al ĉiuj :
http://webchat.freenode.net/?channels=#spip',
	'descriptif_site_spip_mag' => 'Priskribo de la retejo SPIP Magazino',
	'descriptif_site_spip_net' => 'Je la dispono de la uzantoj kaj de la retejestroj, SPIP.net estas la oficiala retejo konsilata por ĉiuj geuzantoj kiuj deziras instali retejon per SPIP, kompreni la lingvaĵo de iteracioj, etikedoj kaj filtriloj, skribi kaj uzi skeletojn. Ĝi prezentas glosaron, lernilojn, konsilojn, historoj pri versioj kaj spaco de elŝutado. SPIP.net estas tradukita al pli ol dudek lingvoj.',
	'descriptif_site_spip_party' => 'Pasigi sian tempon de klavaro al ekrano por kodi SPIP-aĵojn : estas bone. Renkontiĝi inter programistoj kaj uzantoj en reala vivo, nevirtuale, dispartigante kukon kaj fruktosukon (aŭ alian trinkaĵon…) : estas multe pli bone. SPIP-PARTY.net estas la retejo kiu listigas ĉiujn tiujn renkontojn : pasintajn (raportoj, fotoj) kaj venontajn (kalendaro, agendo).',
	'descriptif_site_spip_plugin' => 'PLUGINS.SPIP.net volas esti kompleta aro da aldonendaj moduloj por SPIP (kromprogramoj, skeletoj, grafikaj temoj). Oni prezentas por ĉiu modulo : priskribon, aŭtoron, permesilon, nivelon de kongrueco laŭ la SPIP-versio, lastajn aldonitajn modifojn, staton de tradukado, statistikojn pri uzo, ligilojn por dokumentado kaj enŝutado.',
	'descriptif_site_spip_programmer' => 'Prefere destinita al publiko konsistanta el programistoj aŭ retejestroj kiuj jam havas konojn pri PHP, SQL, HTML, CSS kaj JavaScript, PROGRAMMER.SPIP.net prezentas la plej parto de la SPIP-funkciecoj (API-oj, superregoj, duktoj, …) tra multnombraj kodekzemploj. La retejo proponas elŝute la kompleton de sia enhavo en pdf-formato per libera permesilo cc-by-sa. PROGRAMMER.SPIP.net konsulteblas en la franca, angla kaj hispana.',
	'descriptif_site_spip_sedna' => 'SEDNA.SPIP.net estas RSS-kolektilo pour la tuta SPIP-galakcia aktualeco. La lastaj informoj el pli ol 70 retejoj en rilato kun SPIP tiel facile konsulteblas.',
	'descriptif_site_spip_test' => 'Ĉe GRML.EU, facile instalu vian propran retejon en SPIP per ĝia plej lasta disvolvigata versio. Dank’al kunpartigaj SPIP-kompetentoj : unu nura kerna instalaĵo por multaj retejo, vi estas « hejme », en « via » retejo, kun « via » agordo. Ĝi aparte konvenas por trejnstaĝojn cele al SPIP-uzado.',
	'descriptif_site_spip_trad' => 'La tradukanto-spaco bonvenigas ĉiujn, kiuj deziras helpi la uzant-komunumo de SPIP partoprenante al la traduko de SPIP mem kaj de ĝiaj diversaj kontribuaĵoj.',
	'descriptif_site_spip_user' => 'SPIP@REZO.net estas la interhelpa dissendolisto inter SPIP-uzantoj. La lista arĥivoj konsulteblas ĉe https://www.mail-archive.com/spip@rezo.net/maillist.html aŭ blogforme ĉe http://blog.gmane.org/gmane.comp.web.spip.user.',

	// N
	'nom_boussole_spip' => 'SPIP-Orientigilo',
	'nom_groupe_spip_actualite' => 'Aktualaĵo',
	'nom_groupe_spip_aide' => 'Reciproka helpo',
	'nom_groupe_spip_decouverte' => 'Ekmalkovro',
	'nom_groupe_spip_extension' => 'Kontribuoj',
	'nom_groupe_spip_reference' => 'Dokumentado',
	'nom_site_spip_blog' => 'SPIP Blogo',
	'nom_site_spip_contrib' => 'SPIP-Kontribuo',
	'nom_site_spip_core' => 'SPIP Kerno',
	'nom_site_spip_demo' => 'SPIP Elmontrado',
	'nom_site_spip_doc' => 'SPIP Kodo',
	'nom_site_spip_edgard' => 'Edgard',
	'nom_site_spip_forum' => 'SPIP Forumoj',
	'nom_site_spip_herbier' => 'La Herbaro de SPIP',
	'nom_site_spip_info' => 'SPIP informejo',
	'nom_site_spip_irc' => 'SPIP Tuja Babilejo',
	'nom_site_spip_mag' => 'SPIP Magazino',
	'nom_site_spip_net' => 'SPIP.net',
	'nom_site_spip_party' => 'SPIP Festagendo',
	'nom_site_spip_plugin' => 'Kromprogramoj SPIP',
	'nom_site_spip_programmer' => 'SPIP Programi',
	'nom_site_spip_sedna' => 'Sedna',
	'nom_site_spip_test' => 'SPIP Testi',
	'nom_site_spip_trad' => 'SPIP Traduki',
	'nom_site_spip_twit' => 'SPIP.org',
	'nom_site_spip_user' => 'SPIP Uzantoj',
	'nom_site_spip_video' => 'SPIP Mediateko',
	'nom_site_spip_zine' => 'SPIP Fanzino',
	'nom_site_spip_zone' => 'SPIP Bazaro',

	// S
	'slogan_groupe_spip_actualite' => 'SPIP Aktualaĵoj',
	'slogan_groupe_spip_aide' => 'Helpo kaj interŝanĝoj ĉirkaŭ SPIP',
	'slogan_groupe_spip_decouverte' => 'Ekmalkovro de SPIP',
	'slogan_groupe_spip_extension' => 'Etendigoj kaj kontribuoj al SPIP',
	'slogan_groupe_spip_reference' => 'Referencoj SPIP',
	'slogan_site_spip_blog' => 'Libera programaro kaj tenereco',
	'slogan_site_spip_edgard' => '(Ro)boto pasas kaj ek !',
	'slogan_site_spip_mag' => 'La gazeto pri SPIP',
	'slogan_site_spip_net' => 'La oficiala dokumentado kaj la SPIP elŝute',
	'slogan_site_spip_video' => 'La mediateko de SPIP',
	'slogan_site_spip_zine' => 'La retfanzino pri kaj ĉirkaŭ SPIP'
);
