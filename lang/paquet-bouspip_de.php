<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-bouspip?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouspip_description' => 'Dieses Plugin stellt die Namen, URLs und Beschreibungen aller Websites der SPIP-Galaxis bereit. Es muss mit dem Plugin Kompass als Server verbunden werden, um seine Informationen über einen Webservice an alle Websites zu verteilen, die den SPIP-Kompass anzeigen wollen.',
	'bouspip_nom' => 'SPIP-Kompass',
	'bouspip_slogan' => 'Die besten Adressen der SPIP-Galaxis!'
);
